from xml.etree.ElementTree import ElementTree
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
from xml.dom import minidom
from sets import Set
import json
import os
import StringIO
import csv
import math
from PyANGBasic import *
from PyANGKernel import *
from PyANGGui import *
from PyANGAimsun import *

# Utils -------------------------------------------------------

def find_key_for_value(mylist,xvalue):
    for key, value in mylist.iteritems():
        if value == xvalue:
            return key
    return None

def csv2string(data):
    si = StringIO.StringIO()
    cw = csv.writer(si)
    cw.writerow(data)
    return si.getvalue().strip('\r\n')

def QTimeToSecondsAfterMidnight(qtime):
    return qtime.hour()*3600 + qtime.minute()*60 + qtime.second()

def findIndexFirstZero(A):
    # This function is used to find the first full lane
    for i in range(len(A)):
        if A[i]==0:
            break
    return i

def findIndexLastZero(A):
    for i in range(len(A)-1, -1, -1):
        if A[i]==0:
            break
    return i

def roundToNearest(x,p):
    # This function returns the value in integer times of p
    return float(p * round(x/p))

# Choose scenario to export -----------------------------------
def choose_scenario(model,selection):
    print('Available scenarios: ')
    scenarios = []
    for types in model.getCatalog().getUsedSubTypesFromType(model.getType("GKScenario")):
        for sc in types.itervalues():
            print(str(len(scenarios)) + ') ' + sc.getName())
            scenarios.append(sc)

    if len(scenarios)>0:
        #selection = 3 # get user input here
        scenario = scenarios[selection]
        print('Exporting scenario ' + str(selection) + ':  ' + scenario.getName())
    else:
        scenario = None

    if scenario is not None and scenario.getProblemNet() is not None:
        print('Dont know how to deal with ProblemNet')

    return scenario

# Extract elements --------------------------------------------
def extract_network(model,GKRoadType,export_shapes,type):

    translator=GKCoordinateTranslator(model);

    def remove_upstream_addlane(linkid,fromlane,tolane,section2ignoredaddlane):
        if linkid in section2ignoredaddlane:
            add_lanes = section2ignoredaddlane[linkid]
            full_lanes = link_lanes[linkid]
            left_add_lane  = sum(map(lambda x:x['lanes'],filter(lambda x: x['side']=='l',add_lanes)))
            right_add_lane = sum(map(lambda x:x['lanes'],filter(lambda x: x['side']=='r',add_lanes)))

            lanesMinusOne = tolane - fromlane

            fromlane = min([max([fromlane - left_add_lane,1]),full_lanes])
            tolane = min([fromlane + lanesMinusOne,full_lanes])
        return fromlane,tolane

    def make_dummy_node(section,pS,pE,nodeid):
        # This function is used to create a dummy node 20 ft/meter away from the link end
        L=20
        points = section.calculatePolyline()
        dx = pE.x-pS.x
        dy = pE.y-pS.y
        dn = math.sqrt(dx*dx+dy*dy)
        new_point_x = pS.x - L * dx / dn
        new_point_y = pS.y - L * dy / dn

        xnode = Element('node')
        xnode.set('id',str(nodeid)) 
        xnode.set('x',str(new_point_x)) 
        xnode.set('y',str(new_point_y))

        return xnode

    xnetwork = Element('network')

    # collect roadgeoms ................
    geom_precision = 5.0        # [m]
    addlanes_set = Set([])      # set of unique json
    json2addlaneid = {}         # json to addlanes_id
    section2addlaneid = {}      # section_id to addlanes_id
    section2ignoredaddlane = {} # section_id to ignored_addlane
    c = 0

    # ********* Output all section information ***************
    for types in model.getCatalog().getUsedSubTypesFromType(model.getType("GKSection")):
        for section in types.itervalues():

            # all full lanes
            # If it is not a full lane, it will be ignored for CTM simulation
            if all(map(lambda x: x.isFullLane(),section.getLanes())):
                continue

            add_lanes = []
            ignored_add_lanes = []

            #section.getLanes(): It goes from 0 to N-1 (where N is the number of lanes) 0 is the left most lane,
            # N-1 the right most lane. Left and right using the section direction.
            finalOffset = map(lambda x: x.getFinalOffset(),section.getLanes())      # [m]
            initialOffset = map(lambda x: x.getInitialOffset(),section.getLanes())  # [m]

            ##Note: This method is not applied to the case with 2 or more side lanes (e.g., two left-turn lanes)
            ####### Add lanes
            # exit_left
            if initialOffset[0]!=0:
                # Get the lane lengths in integer times of geo_precision
                pos = roundToNearest(initialOffset[0],geom_precision)
                add_lanes.append( {  'side' : 'l', \
                                     'lanes' : findIndexFirstZero(initialOffset), \
                                     'start_pos': pos, \
                                     'end_pos' : None } )
                ## what is the meaning of "lanes"?
                ## how to differenciate start_pos by exit lane (negative) and entrance lane (positive)??

            # exit_right
            if initialOffset[-1]!=0:
                pos = roundToNearest(initialOffset[-1],geom_precision)
                add_lanes.append( {  'side' : 'r', \
                                     'lanes' : len(initialOffset)-findIndexLastZero(initialOffset)-1, \
                                     'start_pos' : pos , \
                                     'end_pos' : None } )

            ##### Delete lanes
            # entry_left
            if finalOffset[0]!=0:
                pos = roundToNearest(finalOffset[0],geom_precision)
                ignored_add_lanes.append( {  'side' : 'l', \
                                     'lanes' : findIndexFirstZero(finalOffset), \
                                     'start_pos' : None, \
                                     'end_pos' : pos } )
            # entry_right
            if finalOffset[-1]!=0:
                pos = roundToNearest(finalOffset[-1],geom_precision)
                ignored_add_lanes.append( { 'side' : 'r', \
                                     'lanes' : len(finalOffset)-findIndexLastZero(finalOffset)-1, \
                                     'start_pos' : None, \
                                     'end_pos' : pos } )

            if len(add_lanes)!=0:
                addlanes_json = json.dumps(add_lanes)
                if addlanes_json in addlanes_set:
                    section2addlaneid[section.getId()] = json2addlaneid[addlanes_json]
                else:
                    addlanes_set.add(addlanes_json)
                    json2addlaneid[addlanes_json] = c
                    section2addlaneid[section.getId()] = c
                    c = c+1

            # keep the set of upstream addlanes so that we can adjust road connections, etc. 
            if len(ignored_add_lanes)!=0:
                section2ignoredaddlane[section.getId()] = ignored_add_lanes

    xroadgeoms = Element('roadgeoms')
    xnetwork.append(xroadgeoms)
    for k, v in json2addlaneid.items():
        xroadgeom = Element('roadgeom')
        xroadgeoms.append(xroadgeom)
        xroadgeom.set('id',str(v))
        for addlane in json.loads(k):
            xaddlane = Element('add_lanes')
            xroadgeom.append(xaddlane)
            xaddlane.set('side',addlane['side'])
            xaddlane.set('lanes',str(addlane['lanes']))
            if addlane['start_pos'] is not None:
                xaddlane.set('start_pos',str(addlane['start_pos']))
            if addlane['end_pos'] is not None:
                xaddlane.set('end_pos',str(addlane['end_pos']))

    # nodes  ............................
    xnodes = Element('nodes')
    xnetwork.append(xnodes)

    # dummy node
    # xnode = Element('node')
    # xnodes.append(xnode)
    # xnode.set('id','-1') 
    all_node_ids = []
    for types in model.getCatalog().getUsedSubTypesFromType(model.getType("GKNode")):
        for node in types.itervalues():
            xnode = Element('node')
            xnodes.append(xnode)
            xnode.set('id',str(node.getId()))
            # Change the points to longitude and latitude
            point= translator.toDegrees(node.getPosition())
            if(type is 'PointLatLong'):
                xnode.set('x',str(point.x))
                xnode.set('y',str(point.y))
            else:
                xnode.set('x',str(node.getPosition().x))
                xnode.set('y',str(node.getPosition().y))
            all_node_ids.append(node.getId())

    # links  ............................
    link_lanes = {}
    xlinks = Element('links')
    xnetwork.append(xlinks)
    for types in model.getCatalog().getUsedSubTypesFromType(model.getType("GKSection")):
        for section in types.itervalues():

            # The length is in meters
            segmentLength = sum(map(lambda x:section.getSegmentLength(x),range(section.getNbSegments())))

            xlink = Element('link')
            xlinks.append(xlink)
            xlink.set('id',str(section.getId())) 
            xlink.set('length',str(segmentLength))
            xlink.set('full_lanes',str(section.getNbFullLanes()))
            xlink.set('road_type',str(section.getRoadType().getName()))

            xlink.set('end_node_id', \
                str(section.getDestination().getId()) if (section.getDestination() is not None) \
                else '-1' )
            # If need to add lanes
            if section.getId() in section2addlaneid:
                xlink.set('roadgeom',str(section2addlaneid[section.getId()]))

            xlink.set('roadparam',str(section.getRoadType().getId())) 
            link_lanes[section.getId()] = section.getNbFullLanes()

            # For Brian......
            if export_shapes:
                # This gets the shape points
                points = section.calculatePolyline()
                if len(points)>0:
                    xpoints = Element('points')
                    xlink.append(xpoints)
                    for point in points:
                        xpoint = Element('point')
                        xpoints.append(xpoint)
                        if(type is 'PointLatLong'):
                            xpoint.set('x',str(translator.toDegrees(point).x))
                            xpoint.set('y',str(translator.toDegrees(point).y))
                        else:
                            xpoint.set('x',str(point.x))
                            xpoint.set('y',str(point.y))

            # add start dummy nodes if necessary
            if section.getOrigin():
                xlink.set('start_node_id',str(section.getOrigin().getId()))
            else:
                ## dummy node ID: max+1
                new_id = max(all_node_ids)+1
                all_node_ids.append(new_id)
                # new_node = make_dummy_node(section,points[0],points[1],new_id)

                xnode = Element('node')
                xnode.set('id',str(new_id))
                # Start Node: index=0
                if(type is 'PointLatLong'):
                    xnode.set('x',str(translator.toDegrees(points[0]).x))
                    xnode.set('y',str(translator.toDegrees(points[0]).y))
                else:
                    xnode.set('x',str(points[0].x))
                    xnode.set('y',str(points[0].y))
                xlink.set('start_node_id',str(new_id))
                xnodes.append(xnode)

            # add end dummy nodes if necessary
            if section.getDestination():
                xlink.set('end_node_id',str(section.getDestination().getId()))
            else:
                new_id = max(all_node_ids)+1
                all_node_ids.append(new_id)
                # new_node = make_dummy_node(section,points[-1],points[-2],new_id)

                xnode = Element('node')
                xnode.set('id',str(new_id))
                # End Node: index=-1
                if(type is 'PointLatLong'):
                    xnode.set('x',str(translator.toDegrees(points[-1]).x))
                    xnode.set('y',str(translator.toDegrees(points[-1]).y))
                else:
                    xnode.set('x',str(points[-1].x))
                    xnode.set('y',str(points[-1].y))
                xlink.set('end_node_id',str(new_id))
                xnodes.append(xnode)

    # road params .......................
    xroadparams = Element('roadparams')
    xnetwork.append(xroadparams)
    for types in model.getCatalog().getUsedSubTypesFromType(model.getType("GKRoadType")):
        for roadType in types.itervalues():
            if roadType is not None:
                xroadparam = Element('roadparam')
                xroadparams.append(xroadparam)
                xroadparam.set('id',str(roadType.getId()))
                xroadparam.set('name',str(roadType.getDataValueStringByID(GKRoadType.nameAtt)))
                xroadparam.set('capacity',str(roadType.getDataValueDoubleByID(GKRoadType.laneCapacityAtt))) # veh/hr/lane
                xroadparam.set('speed',str(roadType.getDataValueDoubleByID(GKRoadType.speedAtt))) # km/hr
                xroadparam.set('jam_density',str(roadType.getDataValueDoubleByID(GKRoadType.jamDensityAtt))) # veh/km/lane

    # road connections ..................
    xroadconnections = Element('roadconnections')
    xnetwork.append(xroadconnections)
    for types in model.getCatalog().getUsedSubTypesFromType(model.getType("GKTurning")):
        for turn in types.itervalues():
            xrc = Element('roadconnection')
            xroadconnections.append(xrc)
            xrc.set('id',str(turn.getId()))
            # Get the turn length
            turnLength = sum(map(lambda x:turn.getSegmentLength(x),range(turn.getNbSegments())))
            xrc.set('length',str(turnLength))
            xrc.set('in_link',str(turn.getOrigin().getId()))

            # getOriginFromLane(): Gets the left most lane number (from 0 to N-1) used in the origin section by this turn.
            # getOriginToLane(): Gets the right most lane number (from 0 to N-1) used in the origin section by this turn.
            xrc.set('in_link_lanes',str(turn.getOriginFromLane()+1) + '#' + str(turn.getOriginToLane()+1))

            outlink_id = turn.getDestination().getId()
            outlink_fromlane = turn.getDestinationFromLane()+1
            outlink_tolane = turn.getDestinationToLane()+1
            # adjust according if outlink has ignored addlane
            outlink_fromlane,outlink_tolane = remove_upstream_addlane(outlink_id,outlink_fromlane,outlink_tolane,section2ignoredaddlane)
            xrc.set('out_link',str(outlink_id))
            xrc.set('out_link_lanes',str(outlink_fromlane) + '#' + str(outlink_tolane))

    return xnetwork

def extract_sensors(model,scenario):
    if scenario is None:
        return None

    xsensors = Element('sensors')
    for types in model.getCatalog().getUsedSubTypesFromType(model.getType("GKDetector")):
        for detector in types.itervalues():
            xsensor = Element('sensor')
            xsensors.append(xsensor)
            xsensor.set('id',str(detector.getId()))

            # Get external ID: starting with "x"
            if len(str(detector.getExternalId()))!=0:
                xsensor.set('data_id','x'+str(detector.getExternalId()))

            xsensor.set('type','loop')
            xsensor.set('link_id',str(detector.getSection().getId()))
            xsensor.set('position',str(detector.getPosition()))
            xsensor.set('length',str(detector.getLength())) # meter

            # getFromLane(): Lanes in where this object is.
            # getToLane(): Lanes in where this object is.
            xsensor.set('lanes', str(detector.getFromLane()+1) + '#' + str(detector.getToLane()+1))

            xsensor.set('dt','300')
    return xsensors

# This has to be fixed to export actuators related to the mcp
def extract_actuators_1(model,scenario):
    if scenario is None:
        return None

    xactuators = Element('actuators')
    folder = model.getCreateRootFolder().findFolder("GKModel::controlPlans")
    strategy_number = 0 # THIS SHOULD BE REMOVED
    for types in model.getCatalog().getUsedSubTypesFromType(model.getType("GKNode")):
        for node in types.itervalues():

            ctrl = folder[strategy_number].getControlJunction(node.getId())
            if ctrl is None:
                continue 
            if ctrl.getControlJunctionType() == 0: # Not actuated
                continue 
            if len(ctrl.getPhases()) == 0:
                continue

            # collect Aimsun signals
            signals = {}
            for signal in node.getSignals():
                signals[signal.getId()] = signal

            xactuator = Element('actuator')
            xactuators.append(xactuator)

            xactuator.set('id',str(node.getId()))
            xactuator.set('actuator_type','signal')

            xactuatorTarget = Element('actuator_target')
            xactuator.append(xactuatorTarget)
            xactuatorTarget.set('id',str(node.getId()))
            xactuatorTarget.set('type','node')

            xsignal = Element('signal')
            xactuator.append(xsignal)

            # get all unique turns used by this controller
            turns = Set([])
            for phase in ctrl.getPhases():

                if phase.getInterphase():
                    continue
                if len(phase.getSignals())==0:
                    continue

                for sig in phase.getSignals():
                    signal = signals.get(sig.signal)
                    if signal is None:
                        print 'Warning: I did not find signal ' + str(sig.signal) + ' in node ' + str(node.getId()) + ", phase " + str(phase.getId())
                        continue
                    if len(signal.getTurnings())==0:
                        continue
                    for turn in signal.getTurnings():
                        turns.add(turn.getId())

            # make a phase for each one
            for turn in turns:
                xphase = Element('phase')
                xsignal.append(xphase)
                xphase.set('id',str(turn))
                xphase.set('roadconnection_ids',str(turn))

    return xactuators

def extract_actuators_2(model,scenario,GKControlJunction):
    if scenario is None:
        return None

    xactuators = Element('actuators')

    mcp = scenario.getMasterControlPlan()

    # loop through nodes. For each node, loop through all control plans in the schedule
    # and pull out references to that node
    for types in model.getCatalog().getUsedSubTypesFromType( model.getType( "GKNode" ) ):
        for node in types.itervalues():

            node_has_been_found = False
            signals = {}

            for sched_mcp in mcp.getSchedule(): # GKScheduleMasterControlPlanItem

                # retrieve the control junction for this node in this plan, if it exists
                ctrl_jnctn = sched_mcp.getControlPlan().getControlJunction(node) 

                if ctrl_jnctn is None:
                    continue
                if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eUnspecified:
                    continue
                if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eUncontrolled:
                    continue
                if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eExternal:
                    continue
                if len(ctrl_jnctn.getPhases()) == 0:
                    continue

                # collect Aimsun signals
                signals = {}
                for signal in node.getSignals():
                    signals[signal.getId()] = signal

                xactuator = Element('actuator')
                xactuators.append(xactuator)

                xactuator.set('id',str(node.getId()))
                xactuator.set('actuator_type','signal')

                xactuatorTarget = Element('actuator_target')
                xactuator.append(xactuatorTarget)
                xactuatorTarget.set('id',str(node.getId()))
                xactuatorTarget.set('type','node')

                xsignal = Element('signal')
                xactuator.append(xsignal)

                # get all unique turns used by this controller
                turns = Set([])
                for phase in ctrl_jnctn.getPhases():

                    if phase.getInterphase():
                        continue
                    if len(phase.getSignals())==0:
                        continue

                    for sig in phase.getSignals():
                        signal = signals.get(sig.signal)
                        if signal is None:
                            # print 'Warning (extract_actuators): I did not find signal ' + str(sig.signal) + ' in node ' + str(node.getId()) + ", phase " + str(phase.getId())
                            continue
                        if len(signal.getTurnings())==0:
                            continue
                        for turn in signal.getTurnings():
                            turns.add(turn.getId())

                # make a phase for each one
                for turn in turns:
                    xphase = Element('phase')
                    xsignal.append(xphase)
                    xphase.set('id',str(turn))
                xphase.set('roadconnection_ids',str(turn))

                break  # only write each actuator once

    return xactuators

def extract_controllers_1(model,scenario,GKControlJunction):

    if scenario is None:
        return None

    xcontrollers = Element('controllers')

    mcp = scenario.getMasterControlPlan()

    # loop through nodes. For each node, loop through all control plans in the schedule
    # and pull out references to that node
    for types in model.getCatalog().getUsedSubTypesFromType( model.getType( "GKNode" ) ):
        for node in types.itervalues():

            node_has_been_found = False
            signals = {}

            for sched_mcp in mcp.getSchedule(): # GKScheduleMasterControlPlanItem

                # retrieve the control junction for this node in this plan, if it exists
                ctrl_jnctn = sched_mcp.getControlPlan().getControlJunction(node) 

                if ctrl_jnctn is None:
                    continue
                if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eUnspecified:
                    continue
                if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eUncontrolled:
                    continue
                if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eExternal:
                    continue
                if len(ctrl_jnctn.getPhases()) == 0:
                    continue

                # initialize this node
                if not node_has_been_found:
                    node_has_been_found = True
                    xcontroller = Element('controller')
                    xcontrollers.append(xcontroller)
                    xcontroller.set('type','sig_int')

                    # single actuator is the signal for this node
                    xtarget_actuators = Element('target_actuators')
                    xcontroller.append(xtarget_actuators)
                    xtarget_actuator = Element('target_actuator')
                    xtarget_actuators.append(xtarget_actuator)
                    xtarget_actuator.set('id',str(node.getId()))

                    xschedule = Element('schedule')
                    xcontroller.append(xschedule)

                    # collect Aimsun signals
                    for signal in node.getSignals():
                        signals[signal.getId()] = signal

                if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eFixedControl:
                    ctrl_type = 'pretimed'
                if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eActuated:
                    ctrl_type = 'actuated'

                # create a schedule item 
                xsched_item = Element('schedule_item')
                xschedule.append(xsched_item)
                xsched_item.set('start_time',str(sched_mcp.getFrom()))
                xsched_item.set('type',ctrl_type)

                # add stages
                xsignalstages = Element('signal_stages')
                xsched_item.append(xsignalstages)
                xsignalstages.set('cycle',str(ctrl_jnctn.getCycle()))
                xsignalstages.set('offset',str(ctrl_jnctn.getOffset()))
                xsignalstages.set('signal_id',str(node.getId()))

                for phase in ctrl_jnctn.getPhases():

                    if phase.getInterphase():
                        continue
                    if len(phase.getSignals())==0:
                        continue

                    turns = []
                    for sig in phase.getSignals():
                        signal = signals.get(sig.signal)
                        if signal is None:
                            print 'Warning: I did not find signal ' + str(sig.signal) + ' in node ' + str(node.getId()) + ", phase " + str(phase.getId())
                            continue
                        if len(signal.getTurnings())==0:
                            continue
                        for turn in signal.getTurnings():
                            turns.append(turn.getId())

                    if len(turns)==0:
                        continue

                    xstage = Element('stage')
                    xsignalstages.append(xstage)

                    xstage.set('phases',csv2string(turns))
                    xstage.set('start_time',str(phase.getFrom()))
                    xstage.set('duration',str(phase.getDuration()))

    return xcontrollers

def extract_controllers_2(model,scenario,GKControlJunction):

    if scenario is None:
        return None

    xcontrollers = Element('controllers')

    mcp = scenario.getMasterControlPlan()

    # loop through nodes. For each node, loop through all control plans in the schedule
    # and pull out references to that node
    cntrlr_id = 0
    for types in model.getCatalog().getUsedSubTypesFromType( model.getType( "GKNode" ) ):
        for node in types.itervalues():

            node_has_been_found = False
            signals = {}

            for sched_mcp in mcp.getSchedule(): # GKScheduleMasterControlPlanItem

                # retrieve the control junction for this node in this plan, if it exists
                ctrl_jnctn = sched_mcp.getControlPlan().getControlJunction(node) 

                if ctrl_jnctn is None:
                    continue
                if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eUnspecified:
                    continue
                if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eUncontrolled:
                    continue
                if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eExternal:
                    continue
                if len(ctrl_jnctn.getPhases()) == 0:
                    continue

                # if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eFixedControl:
                #     ctrl_type = 'sig_pretimed'
                # if ctrl_jnctn.getControlJunctionType()==GKControlJunction.eActuated:
                #     ctrl_type = 'sig_actuated'

                # initialize this node
                if not node_has_been_found:
                    node_has_been_found = True
                    xcontroller = Element('controller')
                    xcontrollers.append(xcontroller)
                    xcontroller.set('type','sig_pretimed')
                    xcontroller.set('id',str(cntrlr_id))
                    cntrlr_id = cntrlr_id+1

                    # single actuator is the signal for this node
                    xtarget_actuators = Element('target_actuators')
                    xcontroller.append(xtarget_actuators)
                    xtarget_actuators.set('ids',str(node.getId()))
                    xschedule = Element('schedule')
                    xcontroller.append(xschedule)

                    # collect Aimsun signals
                    for signal in node.getSignals():
                        signals[signal.getId()] = signal

                # create a schedule item 
                xsched_item = Element('schedule_item')
                xschedule.append(xsched_item)
                xsched_item.set('start_time',str(sched_mcp.getFrom()))
                xsched_item.set('cycle',str(ctrl_jnctn.getCycle()))
                xsched_item.set('offset',str(ctrl_jnctn.getOffset()))

                # add stages
                xsignalstages = Element('stages')
                xsched_item.append(xsignalstages)

                order = 0
                for phase in ctrl_jnctn.getPhases():

                    if phase.getInterphase():
                        continue
                    if len(phase.getSignals())==0:
                        continue

                    turns = []
                    for sig in phase.getSignals():
                        signal = signals.get(sig.signal)
                        if signal is None:
                            # print 'Warning (extract_controllers): I did not find signal ' + str(sig.signal) + ' in node ' + str(node.getId()) + ", phase " + str(phase.getId())
                            continue
                        if len(signal.getTurnings())==0:
                            continue
                        for turn in signal.getTurnings():
                            turns.append(turn.getId())

                    if len(turns)==0:
                        continue

                    xstage = Element('stage')
                    xsignalstages.append(xstage)

                    order = order+1
                    xstage.set('order',str(order))
                    xstage.set('phases',csv2string(turns))
                    xstage.set('duration',str(phase.getDuration()))

    return xcontrollers

def extract_commodities(model):
    xcommodities = Element('commodities')
    for types in model.getCatalog().getUsedSubTypesFromType( model.getType("GKUserClass") ):
        for userclass in types.itervalues():
            if userclass.getVehicle() is None:
                print 'error: no user class for this demand item'
                continue

            if userclass.getPurpose() is not None:
                print "error: i don't know how to deal with purpose"
                continue

            xcommodity = Element('commodity')
            xcommodities.append(xcommodity)
            xcommodity.set('id',str(userclass.getVehicle().getId()))
            xcommodity.set('name',str(userclass.getVehicle().getId()))
    return xcommodities

def extract_subnetworks(model):
    xsubnetworks = Element('subnetworks')
    for types in model.getCatalog().getUsedSubTypesFromType( model.getType("GKCentroidConfiguration") ):
        for centroid_cfg in types.itervalues():
            routes = centroid_cfg.getRoutes()
            if routes is None:
                continue
            for fldr in routes:
                xsubnetwork = Element('subnetwork')
                xsubnetworks.append(xsubnetwork)
                xsubnetwork.set('id',str(fldr.getId()))
                xsubnetwork.text = csv2string(list(map(lambda x: x.getId(),fldr.getRoute())))
    return xsubnetworks

def extract_ods(model,scenario,GKTrafficDemand,output_path):

    xods = Element('ods')

    if scenario is None:
        return xods

    demand = scenario.getDemand()

    if demand is not None:

        if demand.getProblemNet() is not None: 
            print "I dont know how to deal with non-global ODs"

        if not demand.isValid():
            print "demand is not valid"

        if demand.isEmpty():
            print "demand is empty"

        if demand.overlappedItems():
            print "demand overlapped items"

        print 'getDemand : ' + str(demand.getName()) # GKTrafficDemand


        # OD demand
        if demand.getDemandType()==GKTrafficDemand.eODDemand:

            # demand atttributes
            demand_id = demand.getId()
            # duration = demand.duration().toMilliseconds()/1000.0
            factor = demand.getFactor().toFloat()/100.0
            itime = QTimeToSecondsAfterMidnight(demand.initialTime())

            for scehduleDemandItem in demand.getSchedule():   # GKScheduleDemandItem

                demandItem = scehduleDemandItem.getTrafficDemandItem()  # GKTrafficDemandItem 
                userclass = demandItem.getUserClass()   # GKUserClass
                matrixFilePath = os.path.join(output_path,"odmatrix_" + str(demandItem.getId()) + ".txt")

                # dName = demandItem.getNameAutomatically(False)
                userId = userclass.getVehicle().getId()

                if userclass.getVehicle() is None:
                    print 'error: no user class for this demand item'
                    continue

                if userclass.getPurpose() is not None:
                    print "error: i don't know how to deal with purpose"
                    continue

                xod = Element('od')
                xods.append(xod)
                xod.set('start_time',str(itime))
                # xod.set('duration',str(duration))
                xod.set('scale',str(factor))
                xod.set('input_file',matrixFilePath)
                # xod.set('name',str(dName))
                xod.set('commodity_id',str(userId))
                xod.set('use_all_paths','true')

                # write the matrix
                centroids = demandItem.getCentroidConfiguration().getCentroidsInOrder()
                with open( matrixFilePath, 'w' ) as file:
                    for origin in centroids:
                        for destination in centroids:
                            if origin != destination:
                                trips = demandItem.getTrips( origin, destination )
                                if trips > 0:
                                    file.write( '%u %u %f\n' % (origin.getId(), destination.getId(), trips)  )

        if demand.getDemandType()==GKTrafficDemand.eStateDemand:
            print "I dont know how to deal with traffic states"
    else:
        print "No demand found."

    return xods


def extract_path_demands(model,scenario,GKTrafficDemand,output_path):

    xods = Element('ods')

    if scenario is None:
        return xods

    demand = scenario.getDemand()

    if demand is not None:

        if demand.getProblemNet() is not None: 
            print "I dont know how to deal with non-global ODs"

        if not demand.isValid():
            print "demand is not valid"

        if demand.isEmpty():
            print "demand is empty"

        if demand.overlappedItems():
            print "demand overlapped items"

        print 'getDemand : ' + str(demand.getName()) # GKTrafficDemand


        # OD demand
        if demand.getDemandType()==GKTrafficDemand.eODDemand:

            # demand atttributes
            demand_id = demand.getId()
            # duration = demand.duration().toMilliseconds()/1000.0
            factor = demand.getFactor().toFloat()/100.0
            itime = QTimeToSecondsAfterMidnight(demand.initialTime())

            for scehduleDemandItem in demand.getSchedule():   # GKScheduleDemandItem

                demandItem = scehduleDemandItem.getTrafficDemandItem()  # GKTrafficDemandItem 
                userclass = demandItem.getUserClass()   # GKUserClass
                matrixFilePath = os.path.join(output_path,"odmatrix_" + str(demandItem.getId()) + ".txt")

                # dName = demandItem.getNameAutomatically(False)
                userId = userclass.getVehicle().getId()

                if userclass.getVehicle() is None:
                    print 'error: no user class for this demand item'
                    continue

                if userclass.getPurpose() is not None:
                    print "error: i don't know how to deal with purpose"
                    continue

                xod = Element('od')
                xods.append(xod)
                xod.set('start_time',str(itime))
                # xod.set('duration',str(duration))
                xod.set('scale',str(factor))
                xod.set('input_file',matrixFilePath)
                # xod.set('name',str(dName))
                xod.set('commodity_id',str(userId))
                xod.set('use_all_paths','true')

                # write the matrix
                centroids = demandItem.getCentroidConfiguration().getCentroidsInOrder()
                with open( matrixFilePath, 'w' ) as file:
                    for origin in centroids:
                        for destination in centroids:
                            if origin != destination:
                                trips = demandItem.getTrips( origin, destination )
                                if trips > 0:
                                    file.write( '%u %u %f\n' % (origin.getId(), destination.getId(), trips)  )

        if demand.getDemandType()==GKTrafficDemand.eStateDemand:
            print "I dont know how to deal with traffic states"
    else:
        print "No demand found."

    return xods

