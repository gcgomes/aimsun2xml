from xml.etree.ElementTree import ElementTree
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
from xml.dom import minidom
from sets import Set
import json
import os
import StringIO
import csv

root = os.path.dirname(os.path.dirname(str(model.getDocumentFileName())))
output_path = os.path.join(root,'output')  # output folder for csv files

# import aimsunexport library
home_dir = root  + '\\src'
sys.path.append(home_dir)
import aimsunexport as aimsun

# needed to refresh aimsun cache
reload(aimsun)

# initialize xml
xscenario = Element('scenario')
tree = ElementTree(xscenario)

# Get the active model from Aimsun
model = GKSystem.getSystem().getActiveModel()

# ask the user for the scenario
scenario = aimsun.choose_scenario(model)

# export elements
xsensors = Element('sensors')

for types in model.getCatalog().getUsedSubTypesFromType(model.getType("GKDetector")):
    for detector in types.itervalues():
        xsensor = Element('sensor')
        xsensors.append(xsensor)
        xsensor.set('id',str(detector.getId()))
        if len(str(detector.getExternalId()))!=0:
            xsensor.set('data_id',str(detector.getExternalId()))
        xsensor.set('type','loop')
        xsensor.set('link_id',str(detector.getSection().getId()))
        xsensor.set('position',str(detector.getPosition()))
        xsensor.set('length',str(detector.getLength()))
        xsensor.set('lanes', str(detector.getFromLane()+1) + '#' + str(detector.getToLane()+1))
        xsensor.set('dt','300')

xscenario.append(xsensors)

# write to file
with open(os.path.join(output_path,'bla.xml'), 'w') as f:
    f.write(minidom.parseString(etree.tostring(xscenario)).toprettyxml(indent="\t"))

print "Done"