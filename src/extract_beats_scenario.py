from xml.etree.ElementTree import ElementTree
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
from xml.dom import minidom
from sets import Set
import json
import os

root = os.path.dirname(os.path.dirname(str(model.getDocumentFileName())))
output_path = os.path.join(root,'output')  # output folder for csv files

export_shapes = True
selection=3  # User's input to choose the scenario
type="PointLatLong" # PointXY,PointLatLong

# import aimsunexport library
home_dir = root  + '\\src'
sys.path.append(home_dir)
import aimsunexport as aimsun

# needed to refresh aimsun cache
reload(aimsun)

# initialize xml
xscenario = Element('scenario')
tree = ElementTree(xscenario)

# Get the active model from Aimsun
model = GKSystem.getSystem().getActiveModel()

# ask the user for the scenario
scenario = aimsun.choose_scenario(model,selection)

# export elements
xscenario.append(aimsun.extract_network(model,GKRoadType,export_shapes,type))
xscenario.append(aimsun.extract_sensors(model,scenario))
# xscenario.append(aimsun.extract_actuators(model,scenario))
# xscenario.append(aimsun.extract_controllers(model,scenario,GKControlJunction))
# xscenario.append(aimsun.extract_ods(model,scenario,GKTrafficDemand,output_path))
xscenario.append(aimsun.extract_commodities(model))
xscenario.append(aimsun.extract_subnetworks(model))

# write to file
with open(os.path.join(output_path,'beats_scenario.xml'), 'w') as f:
    f.write(minidom.parseString(etree.tostring(xscenario)).toprettyxml(indent="\t"))

print "Done"